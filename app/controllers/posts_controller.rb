class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  # GET /posts
  # GET /posts.json
  def index
      @posts = Post.where(user_id: current_user.id, published: false).order('id ASC')
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  def import

  end

  def archive
    @posts = Post.where(user_id: current_user.id, published: true).order('id ASC')
  end

  def set_dates
    Post.set_dates(params[:dates], current_user.id)

    redirect_to posts_path
  end

  def upload
    uploaded_io = params[:post][:excell]

    if uploaded_io
      new_name = Time.now.to_i.to_s
      File.open("/tmp/"+uploaded_io.original_filename, "wb") do |file|
        file.write(uploaded_io.read)
      end

      xlsx = Roo::Excelx.new("/tmp/"+uploaded_io.original_filename)

      posts = []
      (1..xlsx.last_row).each do |i|
        posts << {text: xlsx.row(i)[0].to_s}
      end

      Post.create!(posts) do |p|
        p.facebook = true
        p.twitter = true
        p.published = false
        p.user_id = current_user.id
      end

      flash[:notice] = "Upload Successful"
    end

    redirect_to posts_import_path
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:text, :publish_date, :image, :twitter, :facebook, :facebook_publish_date, :twitter_publish_date)
    end
end
