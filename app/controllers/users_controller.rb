class UsersController < ApplicationController
  before_action :set_user, only: [:settings, :update, :publish_token]
  before_filter :authenticate_user!, only: [:show, :edit, :update, :destroy, :settings]

  def update
    if @user.update(user_params)

    end

    redirect_to settings_path
  end

  def auth
    User.from_omniauth(env["omniauth.auth"],current_user)

    redirect_to root_path
  end

  def settings
    @user_facebook_oauth = @user.facebook_oauth_token
  end

  def publish_token

    if params[:facebook] == "profile"
      @user.facebook_publish_token = @user.facebook_oauth_token
      @user.save
    else
      @user.facebook_publish_token = params[:facebook]
      @user.save
    end

    redirect_to settings_path
  end

  private
  def set_user
    @user = User.find(current_user.id)
  end

  def user_params
    params.require(:user).permit(:full_name, :company_name, :email, :password)
  end
end
