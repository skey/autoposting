class Post < ActiveRecord::Base
  has_attached_file :image, :styles => { :original => '1000>', :thumb => '100x100'}
  validates_attachment_content_type :image, :content_type => ["image/jpg","image/jpeg","image/png","image/gif",]
  belongs_to :user

  def self.set_dates(dates, current_user)

    all_posts = Post.where(:user_id => current_user).order('id ASC')
    facebook_next_date = DateTime.parse(dates[:facebook_publish_date]).to_i + dates[:facebook_publish_time].to_i * 60 * 60
    twitter_next_date = DateTime.parse(dates[:twitter_publish_date]).to_i + dates[:twitter_publish_time].to_i * 60 * 60

    all_posts.each do |p|
      if p.facebook?
        facebook_next_date += dates[:facebook_interval].to_i * 60 * 60
        p.facebook_publish_date = Time.at(facebook_next_date).to_datetime
        p.save
      end

      if p.twitter?
        twitter_next_date += dates[:twitter_interval].to_i * 60 * 60
        p.twitter_publish_date = Time.at(twitter_next_date).to_datetime
        p.save
      end

    end
  end

  def self.posting
    posts = Post.where("published = false AND (facebook_publish_date <= CURRENT_TIMESTAMP OR twitter_publish_date <= CURRENT_TIMESTAMP)")
    posts.each do |p|
      if p.twitter?
        unless p.twitter_publish_date.blank?
            client = Twitter::REST::Client.new do |config|
            config.consumer_key = ENV["TWITTER_CONSUMER_KEY"]
            config.consumer_secret = ENV["TWITTER_CONSUMER_SECRET"]
            config.access_token = p.user.twitter_oauth_token
            config.access_token_secret = p.user.twitter_oauth_secret
            end
          if p.image?
            client.update_with_media(p.text, File.new(p.image.path))
          else
            client.update(p.text)
          end
        end
      end

      if p.facebook?
        unless p.facebook_publish_date.blank?
          @graph = Koala::Facebook::API.new(p.user.facebook_publish_token)
          if p.image?
            @graph.put_picture(File.new(p.image.path), {:message => p.text})
          else
            post_info = @graph.put_wall_post(p.text)
          end
      end
      end

      p.published = true
      p.save
  end
  end
end
