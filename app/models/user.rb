class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  has_many :posts

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def self.from_omniauth(auth,current_user)
      user = User.find(current_user.id)
      case auth.provider
        when 'twitter'
          user.twitter_oauth_token = auth.credentials.token
          user.twitter_oauth_secret = auth.credentials.secret
        when 'facebook'
          user.facebook_oauth_token = auth.credentials.token
      end

      user.save!

  end
end
