class AddSocialnetworksToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :facebook, :boolean
    add_column :posts, :twitter, :boolean
  end
end
