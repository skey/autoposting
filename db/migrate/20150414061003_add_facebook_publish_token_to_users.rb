class AddFacebookPublishTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :facebook_publish_token, :string
  end
end
