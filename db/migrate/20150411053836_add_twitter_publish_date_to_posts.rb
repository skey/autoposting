class AddTwitterPublishDateToPosts < ActiveRecord::Migration
  def up
        add_column :posts, :twitter_publish_date, :datetime
  end

  def down
        remove_column :posts, :twitter_publish_date
  end
end
